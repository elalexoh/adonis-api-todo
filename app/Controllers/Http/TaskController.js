"use strict";
const Project = use("App/Models/Project");
const Task = use("App/Models/Task");
const AuthService = use("App/Services/AuthService");

class TaskController {
  async index({ params }) {
    const { id } = params;
    const project = await Project.find(id);
    return await project.tasks().fetch();
  }
  async create({ auth, request, params }) {
    const user = await auth.getUser();
    const { description, done } = request.all();
    const { id } = params;
    const project = await Project.find(id);
    AuthService.verificarPermisos(project, user);
    const task = new Task();
    task.fill({
      description,
      done,
    });
    await project.tasks().save(task);
    return task;
  }
  async destroy({ auth, params }) {
    const user = await auth.getUser();
    const { id } = params;
    const task = await Task.find(id);
    //obtengo el proyecto al que pertenece la tarea
    const project = await task.project().fetch();
    // verifico si el usuario puede borrar la tarea
    AuthService.verificarPermisos(project, user);
    await task.delete();
    return task;
  }
  async update({ auth, params, request }) {
    const user = await auth.getUser();
    const { id } = params;
    const task = await Task.find(id);
    //obtengo el proyecto al que pertenece la tarea
    const project = await task.project().fetch();
    // verifico si el usuario puede borrar la tarea
    AuthService.verificarPermisos(project, user);
    task.merge(request.only(["description", "done"]));
    await task.save();
    return task;
  }
}

module.exports = TaskController;
