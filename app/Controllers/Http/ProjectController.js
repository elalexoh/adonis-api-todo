"use strict";
const Project = use("App/Models/Project");
const AuthService = use("App/Services/AuthService");

class ProjectController {
  async index({ auth }) {
    const user = await auth.getUser();
    return await user.projects().fetch();
  }
  async create({ auth, request }) {
    const user = await auth.getUser();
    const { title } = request.all();
    const project = new Project();
    project.fill({
      title,
    });
    await user.projects().save(project);
    return project;
  }
  async destroy({ auth, response, params }) {
    const user = await auth.getUser();
    const { id } = params;
    const project = await Project.find(id);
    AuthService.verificarPermisos(project, user);
    await project.delete();
    return project;
  }
  async update({ auth, params, request }) {
    const user = await auth.getUser();
    const { id } = params;
    const { title } = request;
    const project = await Project.find(id); //get project to update
    AuthService.verificarPermisos(project, user); //exception handler
    project.merge(request.only("title")); //save only title
    await project.save(); //save in bd
    return project;
  }
}

module.exports = new ProjectController();
